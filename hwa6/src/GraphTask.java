import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 * @since 1.8
 */
public class GraphTask {

    /** Main method. */
    public static void main (String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    public Graph createGraph(){
        return new Graph("G");
    }



    /** Actual main method to run examples and everything. */
    public void run() {
        Graph g = new Graph ("G");

        GraphTask.Vertex vert5 = g.createVertex("v5");
        Vertex vert4 = g.createVertex("v4");
        Vertex vert3 = g.createVertex("v3");
        Vertex vert2 = g.createVertex("v2");
        Vertex vert1 = g.createVertex("v1");

        g.createUndirectedArc("a1-2", vert1, vert2);
        g.createUndirectedArc("a2-3", vert2, vert3);
        g.createUndirectedArc("a2-4", vert2, vert4);
        g.createUndirectedArc("a2-5", vert2, vert5);
        g.createUndirectedArc("a4-5", vert4, vert5);

        System.out.println(g);
//        g.createRandomSimpleGraph (6, 9);
//        System.out.println (g);
        Set<Vertex> maxIndSet = g.getMaxIndependentSet();
        System.out.println(maxIndSet);

    }

    /** C-13.12:
     * An independent set of an undirected graph G = (V,E) is a subset I of V such that no two vertices in I are adjacent.
     * That is, if u and v are in I, then (u,v) is not in E. A maximal independent set M is an independent set such that,
     * if we were to add any additional vertex to M, then it would not be independent any more. Every graph has a maximal
     * independent set. Give an efficient algorithm that computes a maximal independent set for a graph G.
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        // You can add more fields, if needed

        Vertex (String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex (String s) {
            this (s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        public boolean hasNext(){
            return next != null;
        }

        public Vertex getNextVertex(){
            return next;
        }

        public Arc getArc(){
            return first;
        }

    }


    /** Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;
        // You can add more fields, if needed

        Arc (String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc (String s) {
            this (s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        public boolean hasNext(){
            return next!= null;
        }

        public Vertex getTarget(){
            return target;
        }

        public Arc getNextArc(){
            return next;
        }
    }


    /** This header represents a graph.
     */
    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;


        Graph (String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph (String s) {
            this (s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty ("line.separator");
            StringBuffer sb = new StringBuffer (nl);
            sb.append (id);
            sb.append (nl);
            Vertex v = first;
            while (v != null) {
                sb.append (v.toString());
                sb.append (" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append (" ");
                    sb.append (a.toString());
                    sb.append (" (");
                    sb.append (v.toString());
                    sb.append ("->");
                    sb.append (a.target.toString());
                    sb.append (")");
                    a = a.next;
                }
                sb.append (nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex (String vid) {
            Vertex res = new Vertex (vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createUndirectedArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc (aid);
            res.next = from.first;
            from.first = res;
            res.target = to;

            Arc otherWay = new Arc(aid + "<-");
            otherWay.next = to.first;
            to.first = otherWay;
            otherWay.target = from;
            return res;
        }

        public void setFirst(Vertex vertex){
            first = vertex;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         * @param n number of vertices added to this graph
         */
        public void createRandomTree (int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex [n];
            for (int i = 0; i < n; i++) {
                varray [i] = createVertex ("v" + String.valueOf(n-i));
                if (i > 0) {
                    int vnr = (int)(Math.random()*i);
                    createUndirectedArc("a" + varray [vnr].toString() + "_"
                            + varray [i].toString(), varray [vnr], varray [i]);
                    createUndirectedArc("a" + varray [i].toString() + "_"
                            + varray [vnr].toString(), varray [i], varray [vnr]);
                } else {}
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int [info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res [i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph (int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException ("Too many vertices: " + n);
            if (m < n-1 || m > n*(n-1)/2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree (n);       // n-1 edges created here
            Vertex[] vert = new Vertex [n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int)(Math.random()*n);  // random source
                int j = (int)(Math.random()*n);  // random target
                if (i==j)
                    continue;  // no loops
                if (connected [i][j] != 0 || connected [j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert [i];
                Vertex vj = vert [j];
                createUndirectedArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected [i][j] = 1;
                edgeCount--;  // a new edge happily created
            }
        }



        /**
         * Get a maximal independent set of vertices for an undirected graph.
         * @return maximal independent set of vertices Set<Vertex>
         */
        public Set<Vertex> getMaxIndependentSet(){
            List<Set<Vertex>> independentSets = new ArrayList<>();
            Set<Vertex> maxIndependentSet = new HashSet<>();
            if (first == null){
                throw new RuntimeException("Cannot calculate maximum independent set for an empty graph.");
            }
            Vertex currentVertex = first;

            if (!first.hasNext()){
                maxIndependentSet.add(first);
            } else {
                for (int i = 0; i < 2; i++) {
                    independentSets.add(getIndependentSets(first, new HashSet<>(), new HashSet<>()));
                    currentVertex = currentVertex.next;
                }

                int max_size = 0;

                for (Set<Vertex> independentSet : independentSets) {
                    if(independentSet.size() > max_size){
                        max_size = independentSet.size();
                        maxIndependentSet = independentSet;
                    }
                }
            }

            return maxIndependentSet;
        }


// How to join two sets https://stackoverflow.com/questions/9062574/is-there-a-better-way-to-combine-two-string-sets-in-java
        /**
         * Find an independent set starting from given vertex.
         * @param first independent vertices will be gathered starting from this Vertex
         * @param independentVertices set where independentVertices from first will be gathered
         * @param forbiddenVertices set where vertices that are not independent will be gathered
         * @return independent set starting from given vertex first.
         */
        private Set<Vertex> getIndependentSets(Vertex first, Set<Vertex> independentVertices,  Set<Vertex> forbiddenVertices){

            if (independentVertices.isEmpty()){
                independentVertices.add(first);
                forbiddenVertices.addAll(getForbiddenVertices(first.getArc()));
            }

            if (!first.hasNext()) {
                return independentVertices;
            } else {
                if (!forbiddenVertices.contains(first)){
                    independentVertices.add(first);
                    forbiddenVertices.addAll(getForbiddenVertices(first.getArc()));
                }
                return Stream.concat(independentVertices.stream(), getIndependentSets(first.getNextVertex(), independentVertices, forbiddenVertices).stream())
                        .collect(Collectors.toSet());
            }

            }

        /**
         * Find all vertices that cannot be independent for one vertex.
         * @param arc first of the arcs that will reveal vertices that cannot be independent for one vertex
         * @return set of vertices that cannot be independent for one vertex
         */
        private Set<Vertex> getForbiddenVertices(Arc arc) {
            Arc currentArc = arc;
            Set<Vertex> forbSet = new HashSet<>();

            boolean flag = true;

            while (flag){
                if (!currentArc.hasNext()){
                    flag = false;
                }
                forbSet.add(currentArc.target);
                if (flag){
                    currentArc = currentArc.getNextArc();
                }

            }


            return forbSet;
        }

    }
}


