import static org.junit.Assert.*;
import org.junit.Test;

import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

    @Test (timeout=20000)
    public void testMaxIndependentSetGraph1() {
        GraphTask gT = new GraphTask();
        GraphTask.Graph g = gT.createGraph();

        GraphTask.Vertex vert4 = g.createVertex("v4");
        GraphTask.Vertex vert3 = g.createVertex("v3");
        GraphTask.Vertex vert2 = g.createVertex("v2");
        GraphTask.Vertex vert1 = g.createVertex("v1");

        g.createUndirectedArc("a1-2", vert1, vert2);
        g.createUndirectedArc("a2-3", vert2, vert3);
        g.createUndirectedArc("a3-4", vert3, vert4);
        g.createUndirectedArc("a4-1", vert4, vert1);


        Set<GraphTask.Vertex> expectedSet = new HashSet<>();
        expectedSet.add(vert1);
        expectedSet.add(vert3);

        assertEquals(expectedSet, g.getMaxIndependentSet());

    }

    @Test (timeout=20000)
    public void testMaxIndependentSetGraph2(){
        GraphTask gT = new GraphTask();
        GraphTask.Graph g = gT.createGraph();

        GraphTask.Vertex vert5 = g.createVertex("v5");
        GraphTask.Vertex vert4 = g.createVertex("v4");
        GraphTask.Vertex vert3 = g.createVertex("v3");
        GraphTask.Vertex vert2 = g.createVertex("v2");
        GraphTask.Vertex vert1 = g.createVertex("v1");

        g.createUndirectedArc("a1-2", vert1, vert2);
        g.createUndirectedArc("a2-3", vert2, vert3);
        g.createUndirectedArc("a2-4", vert2, vert4);
        g.createUndirectedArc("a2-5", vert2, vert5);
        g.createUndirectedArc("a4-5", vert4, vert5);


        Set<GraphTask.Vertex> expectedSet = new HashSet<>();
        expectedSet.add(vert1);
        expectedSet.add(vert3);
        expectedSet.add(vert4);

        assertEquals(expectedSet, g.getMaxIndependentSet());

    }

    @Test (timeout=20000)
    public void testMaxIndependentSetWithOneVertex(){
        GraphTask gT = new GraphTask();
        GraphTask.Graph g = gT.createGraph();

        GraphTask.Vertex vert1 = g.createVertex("v1");

        Set<GraphTask.Vertex> expectedSet = new HashSet<>();
        expectedSet.add(vert1);

        assertEquals(expectedSet, g.getMaxIndependentSet());

    }

    @Test (timeout=20000)
    public void testMaxIndependentSetGraphWithTwoVertixes(){
        GraphTask gT = new GraphTask();
        GraphTask.Graph g = gT.createGraph();

        GraphTask.Vertex vert2 = g.createVertex("v2");
        GraphTask.Vertex vert1 = g.createVertex("v1");

        g.createUndirectedArc("a1-2", vert1, vert2);

        Set<GraphTask.Vertex> expectedSet = new HashSet<>();
        expectedSet.add(vert1);

        assertEquals(expectedSet, g.getMaxIndependentSet());

    }

    @Test (expected=RuntimeException.class)
    public void testEmpty1() {
        GraphTask gT = new GraphTask();
        GraphTask.Graph g = gT.createGraph();
        g.getMaxIndependentSet();
    }

    @Test (timeout=20000)
    public void test2000Vertices(){
        GraphTask gT = new GraphTask();
        GraphTask.Graph g = gT.createGraph();
        g.createRandomSimpleGraph(2000, 2000);

        long stime, ftime, diff;
        stime = System.nanoTime();
        g.getMaxIndependentSet();
        ftime = System.nanoTime();
        diff = ftime - stime;
        System.out.printf("%34s%11d%n", "Maximal independent set: time (ms): ", diff / 1000000);

    }

}
