import static org.junit.Assert.*;
import org.junit.Test;

public class NewLfractionPowTests {

    @Test
    public void TestPow0(){

        // Arrange
        Lfraction f1 = new Lfraction (2, 5);

        // Act
        Lfraction result = f1.pow(0);

        // Assert
        assertEquals(new Lfraction(1, 1), result);
    }

    @Test
    public void TestPow1(){

        // Arrange
        Lfraction f1 = new Lfraction (2, 5);

        // Act
        Lfraction result = f1.pow(1);

        // Assert
        assertEquals(f1, result);
    }

    @Test
    public void TestPowMinus1(){

        // Arrange
        Lfraction f1 = new Lfraction (2, 5);

        // Act
        Lfraction result = f1.pow(-1);

        // Assert
        assertEquals(f1.inverse(), result);
    }

    @Test
    public void TestPowN(){

        // Arrange
        Lfraction f1 = new Lfraction (2, 5);
        int n = 3;

        // Act
        Lfraction result = f1.pow(n);

        // Assert
        assertEquals(f1.times(f1.pow(n-1)), result);
    }

    @Test
    public void TestPowMinusN(){

        // Arrange
        Lfraction f1 = new Lfraction (2, 5);
        int n = 3;

        // Act
        Lfraction result = f1.pow(-n);

        // Assert
        assertEquals(f1.pow(n).inverse(), result);
    }
}