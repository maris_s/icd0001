import static org.junit.Assert.*;
import org.junit.Test;

public class NewDoubleStackTests {

    public static double delta = 0.000000001;

    @Test (timeout=1000)
    public void TestSwap(){

        // Arrange
        String s = "2 5 SWAP -";

        // Act
        double result = DoubleStack.interpret(s);

        // Assert
        assertEquals("expression: " + Aout.toString (s), 3., result, delta);
    }

    @Test (timeout=1000)
    public void TestDup(){

        // Arrange
        String s = "3 DUP *";

        // Act
        double result = DoubleStack.interpret(s);

        // Assert
        assertEquals("expression: " + Aout.toString (s), 9., result, delta);
    }

    @Test (timeout=1000)
    public void TestRot(){

        // Arrange
        String s = "2 5 9 ROT - +";

        // Act
        double result = DoubleStack.interpret(s);

        // Assert
        assertEquals("expression: " + Aout.toString (s), 12., result, delta);
    }

    @Test
    public void TestRotSwapDup(){

        // Arrange
        String s = "-3 -5 -7 ROT - SWAP DUP * +" ;

        // Act
        double result = DoubleStack.interpret(s);

        // Assert
        assertEquals("expression: " + Aout.toString (s), 21., result, delta);
    }

    @Test (expected=RuntimeException.class)
    public void TestSwapWrongString(){
        DoubleStack.interpret("3 SWAP -");
    }

    @Test (expected=RuntimeException.class)
    public void TestDupWrongString(){
        DoubleStack.interpret("3 4 DUP *");
    }

    @Test (expected=RuntimeException.class)
    public void TestRotWrongString(){
        DoubleStack.interpret("2 5 ROT - +");
    }

    @Test (expected=RuntimeException.class)
    public void TestAllWrongString(){
        DoubleStack.interpret("-3 -5 ROT - SWAP DUP +");
    }

}