import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    /** Main method. Different tests. */
    public static void main (String[] param) {
        // TODO!!! Your debugging tests here
    }

    private long numerator;
    private long denominator;

    /** Constructor.
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction (long a, long b) {
        if (b > 0){
            this.numerator = a;
        } else if (b == 0){
            throw new IllegalArgumentException("The denominator cannot be 0.");
        } else {
            this.numerator = a * -1;
        }

        this.denominator = Math.abs(b);
        reduceFraction();
    }


    private void reduceFraction(){
        Long commonDivisor = getLargestCommonDivisor(numerator, denominator);

        numerator = numerator / commonDivisor;
        denominator = denominator / commonDivisor;
    }

    private static Long getLargestCommonDivisor(Long num, Long denom){
        long divisor = 1L;

        for (int i = 1; i <= Math.abs(num) && i<= denom; i++) {
            if (num % i == 0 && denom % i == 0){
                divisor = (long) i;
            }
        }

        return divisor;
    }

    /** Public method to access the numerator field.
     * @return numerator
     */
    public long getNumerator() {
        return numerator;
    }

    /** Public method to access the denominator field.
     * @return denominator
     */
    public long getDenominator() {
        return denominator;
    }

    /** Conversion to string.
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    /** Equality test.
     * @param m second fraction
     * @return true if fractions this and m are equal
     */

    //https://www.geeksforgeeks.org/overriding-equals-method-in-java/
    @Override
    public boolean equals (Object m) {
        // If the object is compared with itself then return true
        if (m == this) {
            return true;
        }

        /* Check if m is an instance of Lfraction or not
          "null instanceof [type]" also returns false */
        if (!(m instanceof Lfraction)) {
            return false;
        }

        // typecast m to Lfraction so that we can compare data members
        Lfraction c = (Lfraction) m;

        // Compare the data members and return accordingly
        return this.numerator * c.denominator == this.denominator * c.numerator;
    }

    /** Hashcode has to be the same for equal fractions and in general, different
     * for different fractions.
     * @return hashcode
     */
    //https://mkyong.com/java/java-how-to-overrides-equals-and-hashcode/
    @Override
    public int hashCode() {
        return Objects.hash(numerator, denominator);
    }

    /** Sum of fractions.
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus (Lfraction m) {
        long num = numerator * m.denominator + m.numerator * denominator;
        long denom = denominator * m.denominator;

        if (num == 0){
            return new Lfraction(num, 1);
        }

        return new Lfraction(num, denom);
    }

    /** Multiplication of fractions.
     * @param m second factor
     * @return this*m
     */
    public Lfraction times (Lfraction m) {
        if (numerator == 0){
            return new Lfraction(numerator, denominator);
        } if (m.numerator == 0){
            return new Lfraction(m.numerator, m.denominator);
        }

        return new Lfraction(numerator * m.numerator, denominator * m.denominator);
    }

    /** Inverse of the fraction. n/d becomes d/n.
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if (numerator == 0L){
            throw new IllegalArgumentException("Operation cannot be performed as denominator cannot be zero.");
        }

        return new Lfraction(denominator, numerator);
    }

    /** Opposite of the fraction. n/d becomes -n/d.
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        return new Lfraction(numerator * -1, denominator);
    }

    /** Difference of fractions.
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus (Lfraction m) {
        long num = numerator * m.denominator - m.numerator * denominator;
        long denom = denominator * m.denominator;

        if (num == 0){
            return new Lfraction(num, 1);
        }

        return new Lfraction(num, denom);
    }

    /** Quotient of fractions.
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy (Lfraction m) {
        if (m.numerator == 0){
            throw new IllegalArgumentException("Operation cannot be performed as denominator cannot be zero.");
        }

        Lfraction inverseFrac = m.inverse();

        return times(inverseFrac);
    }

    /** Comparision of fractions.
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    //http://csc.columbusstate.edu/woolbright/java/compare.html
    @Override
    public int compareTo (Lfraction m) {
        if (this.numerator * m.denominator > this.denominator * m.numerator) {
            // if current object is greater,then return 1
            return 1;
        }
        else if (this.numerator * m.denominator < this.denominator * m.numerator) {
            // if current object is smaller,then return -1
            return -1;
        }
        else {
            // if current object is equal to m,then return 0

            return 0;
        }
    }



    /** Clone of the fraction.
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Lfraction(numerator, denominator);
    }

    /** Integer part of the (improper) fraction.
     * @return integer part of this fraction
     */
    public long integerPart() {
        return numerator / denominator ;
    }

    /** Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        long integerPart = integerPart();
        if (integerPart == 0){
            return new Lfraction(numerator, denominator);
        }

        long newNum = numerator % denominator;

        return new Lfraction(newNum, denominator);
    }

    /** Approximate value of the fraction.
     * @return real value of this fraction
     */
    public double toDouble() {
        return (double) numerator / (double) denominator;
    }

    /** Double value f presented as a fraction with denominator d > 0.
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction (double f, long d) {
        long newNum = (long) Math.ceil((d * f));

        return new Lfraction(newNum, d);
    }

    /** Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf (String s) {
        int slashCount = 0;
        for (char letter : s.toCharArray()) {
            if (letter == '/'){
                slashCount += 1;
            }
        }

        if (slashCount != 1){
            throw new IllegalArgumentException("The argument is not a valid fraction: " + s);
        }

        String[] split = s.split("/");
        if (split.length != 2){
            throw new IllegalArgumentException("Given argument is invalid: " + s);
        }

        if (!isLong(split[0].trim()) || !isLong(split[1].trim())){
            throw new IllegalArgumentException("Given argument is invalid: " + s);
        }

        long num = Long.parseLong(split[0].trim());
        long denom = Long.parseLong(split[1].trim());
        return new Lfraction(num, denom);
    }

    public static boolean isLong(String input) {
        try {
            Long.parseLong( input );
            return true;
        }
        catch( Exception e ) {
            return false;
        }
    }

    public Lfraction pow(int pow){
        if (pow == 0){
            return new Lfraction(1, 1);
        } if (pow > 0){
            return new Lfraction((long) Math.pow(numerator, pow), (long) Math.pow(denominator, pow));
        }
        if (numerator == 0){
            throw new RuntimeException("Denominator cannot be 0.");
        }
        return new Lfraction((long) Math.pow(denominator, Math.abs(pow)), (long) Math.pow(numerator, Math.abs(pow)));
    }
}


