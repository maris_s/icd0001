import java.util.*;

/** Stack manipulation.
 * @since 1.8
 */
public class DoubleStack {

    private LinkedList<Double> list;

    public static void main (String[] argum) {
        // TODO!!! Your tests here!
    }

    DoubleStack() {
        list = new LinkedList<>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {

        DoubleStack clonedDoubleStack = new DoubleStack();
        LinkedList<Double> clonedList = new LinkedList<>();


        for (Object o : list) {
            clonedList.add((Double) o);
        }

        clonedDoubleStack.list = clonedList;

        return clonedDoubleStack;
    }

    public boolean stEmpty() {

        return list.isEmpty();
    }

    public void push (double a) {
        list.addLast(a);
    }

    public double pop() {
        if (stEmpty()){
            throw new ArrayIndexOutOfBoundsException("The stack is empty.");
        }

        return list.removeLast();
    }

    public void op (String s) {
        if (list.size() < 2){
            throw new ArrayIndexOutOfBoundsException("The stack does not have enough elements.");
        }
        double result;

        double elem2 = pop();
        double elem1 = pop();
        switch(s){
            case "+":
                result = elem1 + elem2;
                break;
            case "-":
                result = elem1 - elem2;
                break;
            case "*":
                result = elem1 * elem2;
                break;
            case "/":
                result = elem1 / elem2;
                break;
            default:
                throw new IllegalArgumentException("It is not possible to do an operation with argument: " + s);
        }
        push(result);

    }

    public double tos() {
        if (stEmpty()){
            throw new ArrayIndexOutOfBoundsException("The stack is empty.");
        }
        return list.getLast();
    }

    //https://www.geeksforgeeks.org/overriding-equals-method-in-java/
    @Override
    public boolean equals (Object o) {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof DoubleStack)) {
            return false;
        }

        // typecast o to Complex so that we can compare data members
        DoubleStack c = (DoubleStack) o;

        // Compare the data members and return accordingly
        return list.equals(c.list);
    }

    @Override
    public String toString() {
        StringBuffer s = new StringBuffer();

        if (!stEmpty()){
            for (Double elem : list) {
                s.append(elem);
            }
        }
        return String.valueOf(s);
    }

    public static double interpret (String pol) {
        if (pol.isEmpty() || pol == null){
            throw new IllegalArgumentException("The argument is null or empty. Argument provided was: " + pol);
        }
        DoubleStack doubleStack = new DoubleStack();

        String[] arguments = pol.split(" ");

        for (String arg : arguments) {
            String trimmedArg = arg.trim();

            if (isDouble(trimmedArg)){
                doubleStack.push(Double.parseDouble(trimmedArg));
            } else if (trimmedArg.equals("+") || trimmedArg.equals("-") || trimmedArg.equals("*") || trimmedArg.equals("/")) {
                if (doubleStack.list.size() < 2) {
                    throw new IllegalArgumentException("Too many operators. Argument provided was: " + pol);
                }
                doubleStack.op(trimmedArg);
            } else if (trimmedArg.equals("SWAP")) {
                if (doubleStack.list.size() < 2) {
                    throw new IllegalArgumentException("There are not enough items in the stack to perform SWAP: " + pol);
                }
                doubleStack.swap();
            } else if (trimmedArg.equals("DUP")) {
                if (doubleStack.list.size() < 1) {
                    throw new IllegalArgumentException("There are not enough items in the stack to perform DUP: " + pol);
                }
                doubleStack.dup();
            } else if(trimmedArg.equals("ROT")){
                if (doubleStack.list.size() < 3) {
                    throw new IllegalArgumentException("There are not enough items in the stack to perform ROT: " + pol);
                }
                doubleStack.rot();
            } else if (trimmedArg.isEmpty()){
                continue;
            } else {
                throw new IllegalArgumentException("This symbol: " + trimmedArg + " is not allowed. Argument provided was: " + pol);
            }
        }

        if (doubleStack.list.size() > 1){
            throw new IllegalArgumentException("Too many operands. Argument provided was: " + pol);
        }

        return doubleStack.tos();
    }

    public void swap(){
        Double lastItem = this.pop();
        Double secondLast = this.pop();
        this.push(lastItem);
        this.push(secondLast);
    }

    public void dup(){
        double item = list.getLast();
        this.push(item);
    }

    public void rot(){
        Double last = this.pop();
        Double secondLast = this.pop();
        Double thirdLast = this.pop();

        this.push(secondLast);
        this.push(last);
        this.push(thirdLast);
    }

    //https://stackoverflow.com/questions/51193994/how-to-determine-on-java-if-a-string-is-a-double-or-not
    private static boolean isDouble(String string)
    {
        try
        {
            Double.parseDouble(string);
        }
        catch (NumberFormatException e)
        {
            return false;
        }
        return true;
    }

}