import java.util.*;

/** Tree with two pointers.
 * @since 1.8
 */
public class Tnode {

    private String name;
    private Tnode firstChild;
    private Tnode nextSibling;
    private boolean isRoot = false;


    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        b.append(name);
        if (isRoot && hasChild()) {
            b.append("(");
            b.append(firstChild.toString());
            b.append(")");
        } else if (hasChild()){
            b.append("(");
            b.append(firstChild.toString());
            b.append(")");
        } if (hasNextSibling()){
            b.append(",");
            b.append(nextSibling.toString());
        }
        return b.toString();
    }

    // help from pseudocode https://www.chegg.com/homework-help/questions-and-answers/expression-trees-goal-task-write-java-program-reads-text-file-one-math-expression-per-line-q29302756
    public static Tnode buildFromRPN (String pol) {
        if (pol.isEmpty()){
            throw new IllegalArgumentException("Cannot form a RPN tree from an empty input.");
        }

        Stack<Tnode> treeNodes = new Stack<>();

        String[] splittedPol = pol.split(" ");

        for (String item : splittedPol){
            if(isInteger(item)){
                Tnode leaf = new Tnode();
                leaf.name = item;
                treeNodes.push(leaf);
            } else if (item.equals("+") || item.equals("-") || item.equals("*") || item.equals("/")){
                if (treeNodes.size() >= 2){
                    Tnode secondChild = treeNodes.pop();
                    Tnode firstChild = treeNodes.pop();
                    firstChild.nextSibling = secondChild;
                    Tnode leaf = new Tnode();
                    leaf.name = item;
                    leaf.firstChild = firstChild;
                    treeNodes.push(leaf);
                } else {
                    throw new IllegalArgumentException("The argument provided is not in the suitable format for RPN: " + pol);
                }

            } else if (item.equals("SWAP")){
                if (treeNodes.size() >= 2){
                    Tnode first = treeNodes.pop();
                    Tnode second = treeNodes.pop();
                    treeNodes.push(first);
                    treeNodes.push(second);
                } else {
                    throw new IllegalArgumentException("There are not enough elements to perform SWAP: " + pol);
                }
            } else if (item.equals("ROT")) {
                if (treeNodes.size() >= 3) {
                    Tnode first = treeNodes.pop();
                    Tnode second = treeNodes.pop();
                    Tnode third = treeNodes.pop();
                    treeNodes.push(second);
                    treeNodes.push(first);
                    treeNodes.push(third);
                } else {
                    throw new IllegalArgumentException("There are not enough elements to perform ROT: " + pol);
                }
            } else if (item.equals("DUP")){
                if (treeNodes.size() >= 1){
                    Tnode clone = new Tnode();
                    clone.name = treeNodes.peek().name;
                    clone.firstChild = treeNodes.peek().firstChild;
                    clone.nextSibling = treeNodes.peek().nextSibling;
                    treeNodes.push(clone);

                } else {
                    throw new IllegalArgumentException("There are not enough elements to perform ROT: " + pol);
                }
            } else {
                throw new IllegalArgumentException("The following symbol is not allowed as part of RPN: " + item + ". The argument that was provided: " + pol);
            }
        }
        if (treeNodes.size() != 1){
            throw new IllegalArgumentException("The argument doesn't have suitable number of elements for RPN: " + pol);
        }
        Tnode root = treeNodes.pop();
        root.isRoot = true;
        return root;
    }

    //https://stackoverflow.com/questions/5439529/determine-if-a-string-is-an-integer-in-java
    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException | NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    private boolean hasNextSibling(){
        return nextSibling != null;
    }

    private boolean hasChild(){
        return firstChild != null;
    }

    public static void main (String[] param) {
        String rpn = "2 5 DUP ROT - + DUP *";
        System.out.println ("RPN: " + rpn);
        Tnode res = buildFromRPN (rpn);
        System.out.println ("Tree: " + res);
        // TODO!!! Your tests here
    }
}
