import java.util.Arrays;

/** Sorting of balls.
 * @since 1.8
 */

public class ColorSort {

    enum Color {red, green, blue};

    public static void main (String[] param) {
        // for debugging
    }

    public static void reorder (Color[] balls) {
        // The basis taken from https://www.geeksforgeeks.org/counting-sort/ but changes made according to our assignment.

        int max = 2; // maximum value of enum Color
        int min = 0; // minimum value of enum Color
        int range = max - min + 1;
        int count[] = new int[range]; // frequency of each color will be counted here
        Color sorted[] = new Color[balls.length];
        for (int i = 0; i < balls.length; i++) {
            count[balls[i].ordinal() - min]++; //increases the count of the colour in count array
        }

        for (int i = 1; i < count.length; i++) {
            count[i] += count[i - 1]; // adds together counts in count array
        }

        for (int i = balls.length - 1; i >= 0; i--) {
            sorted[count[balls[i].ordinal() - min] - 1] = balls[i]; //place the colors in correct positions
            count[balls[i].ordinal() - min]--; //decrease the respective count by one
        }

        System.arraycopy(sorted, 0, balls, 0, balls.length);
       /*  for (int i = 0; i < balls.length; i++) {
            balls[i] = sorted[i];
        } */
    }
}
