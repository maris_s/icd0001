
import java.util.*;

/** Word puzzle.
 * @since 1.8
 */
public class Puzzle {

    /** Solve the word puzzle.
     * @param args three words (addend1 addend2 sum)
     */

    public static void main (String[] args) {
        if (args.length != 3){
            throw new IllegalArgumentException("Argument provided must include 3 words. Argument provided was: " + Arrays.toString(args));
        }

        if (!argumentsAreValidSize(args)){
            throw new IllegalArgumentException("Each word provided cannot be more than 18 letters. Argument provided was: " + Arrays.toString(args));
            }
        int result = getNumberOfSolutions(args);

    }

    private static int getNumberOfSolutions(String[] args) {
        String[] places = new String[10];
        HashSet<Character> uniqueLetters = getUniqueLetters(args);
        if (uniqueLetters.size() > 10){
            throw new IllegalArgumentException("It is not possible to find a solution for over 10 unique letters: ");
        }
        int numsol = 0;
        int n = 0;
        for (Character letter : uniqueLetters) {

        }

        System.out.println ("Finished with " + numsol + " solutions");
        return numsol;
    }


    private static HashSet<Character> getUniqueLetters(String[] args){
        HashSet<Character> uniqueLetters = new HashSet<>();
        for (String arg : args) {
            for (Character letter : arg.toCharArray()) {
                uniqueLetters.add(letter);
            }
        }
        return uniqueLetters;
    }

    private static boolean argumentsAreValidSize(String[] args){
        for (String arg : args) {
            if (arg.length() > 18 || arg.length() == 0){
                return false;
            }
        }
        return true;
    }
}
